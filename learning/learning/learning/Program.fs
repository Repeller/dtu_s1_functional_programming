﻿// Learn more about F# at http://fsharp.org

open System

let hello() = 
    let big_pi = 3.14159265359252312512M

    //printfn "PI : %f" 3.14159265359
    //printfn "PI : %.4f" 3.14159265359
    
    //printfn "Big Pi : %M" big_pi

    // padding
    //printfn "%-5s %5s" "a" "b"

    // dinamaic padding
    printfn "%*s" 35 "hi"


hello()

Console.ReadKey() |> ignore

//[<EntryPoint>]
//let main argv =
    //printfn "Hello World from F#!"
    //0 // return an integer exit code
